import numpy as np
import matplotlib.pyplot as plt

def parsimonia_LS(vk,b,maxgp,stderr=0.0):
  # "A": matrice del modello lineare
  n = len(vk)
  Ai = np.vander(vk)
  n2res = np.zeros(maxgp+1)
  estvar_errpred = np.zeros(maxgp+1)
  estvar_parest = np.zeros((maxgp+1,maxgp+1))
  for gradopol in range(maxgp+1):
    A = Ai[:,range(n-(gradopol+1),n)]
    #print "A.shape: ",A.shape
    Qh,Rh = np.linalg.qr(A.copy())
    x = np.linalg.solve( Rh , Qh.T@b )
    n2res[gradopol] = np.linalg.norm(A@x - b, 2)
    mmn = A.shape[0] - A.shape[1]
    if mmn>0:   # altrimenti non sono piu' nel caso dei minimi quadrati
      estvar_errpred[gradopol] = n2res[gradopol]**2 / mmn
    #endif
    covd = np.diag(estvar_errpred[gradopol]*(np.linalg.inv(A.T@A)))
    estvar_parest[gradopol,0:gradopol+1] = covd
    plt.figure(10+gradopol); plt.plot(vk,b,'bo'); plt.plot(vk,A@x,'mo'); plt.title('grado = '+str(gradopol)); plt.show()
    B = np.concatenate((A, b), axis=1)
    #print "B.shape: ",B.shape
    U,S,V = np.linalg.svd(B)
    #V = V.T  # NB: per riportarsi a quella solita, basta scommentare questa istruzione
    print("parametri stimati: ", x.T)
    print("varianza delle stime dei parametri: ", covd)
    print("valori singolari di [A b]: ",S)
  #endfor
  plt.figure(40); plt.plot(n2res); plt.title('norma-2 del residuo')
  plt.figure(50); plt.plot(estvar_errpred,'b-'); plt.plot(np.array([0., maxgp]),np.array([stderr**2, stderr**2]),'r-'); plt.title('varianza rumore di misura (rosso) e stima (blu)');
           
           